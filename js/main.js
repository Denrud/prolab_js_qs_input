// taska - 1 show & hidden modal window when button was pressed. +
const blur = document.querySelector(".content-box>p");
const modal = document.querySelector(".modal-window");
const handlerBtn = document.querySelector(".btn-modal");
const scroll = document.querySelector(".content-box");

handlerBtn.addEventListener('click', () => {
    if (blur.className !== "blur" && handlerBtn.innerText !== "close") {
        blur.classList.add("blur");
        modal.classList.remove("hidden");
        handlerBtn.innerText = "hide";
        scroll.style.overflowY = "hidden";
    }
    else {
        blur.classList.remove("blur");
        modal.classList.add("hidden");
        handlerBtn.innerText = "show";
        scroll.style.overflowY = "auto";
    }
})


/* taska - 2
    1. close / open modals and form +
    2. change text in centre block + 
    3. print text from input area "f-name, s-name, age" +
    4. add the picture to block section on front-side from input area
*/

const closeModalForm = document.querySelector("body");
const openModal = document.querySelector('.button-form');
const modalForm = document.querySelector('.handler-interface>form');
const textArea = document.querySelector('.text-area>form');
console.log(textArea);
console.log( document.querySelector('.user-name') );

modalForm.addEventListener('submit', (ev) => {
    ev.preventDefault();
    const getData = new FormData(modalForm);
    const nameKey = getData.get('name');
    const lastNameKey = getData.get('last-name');
    const ageKey = getData.get('age');
    document.querySelector('.user-name').innerText = nameKey;
    document.querySelector('.user-last-name').innerText = lastNameKey;
    document.querySelector('.user-age').innerText = ageKey;
    document.querySelector('.initials').classList.remove('not-edited');    
})

textArea.addEventListener('submit', (ev) => {
    ev.preventDefault();
    const getData = new FormData(textArea);
    const textAreaData = getData.get('text-area-data');
    document.querySelector('.content-box>p').innerText = textAreaData;
})

openModal.addEventListener('click', () => {
    if ( document.querySelector('body').className !== "open" ) {
        closeModalForm.classList.add("open");
        document.querySelector('.container').classList.add('modal-bg');
        document.querySelector('.handler-interface').classList.remove('hidden');
        document.querySelector('.initials').classList.add('blur');
        document.querySelector('.user-avatar>img').classList.add('blur');
        document.querySelector('.content-box').classList.add('text-blur');
        document.querySelector('.button-form').setAttribute("disabled", "disabled");
        document.querySelector('.button-form').classList.add('blur-button');
        handlerBtn.innerText = "close";
    }
})

handlerBtn.addEventListener('click', () => {
    if (document.querySelector('body').className === 'open') {
        document.querySelector('body').classList.remove('open');
        document.querySelector('.container').classList.remove('modal-bg');
        document.querySelector('.handler-interface').classList.add('hidden');
        document.querySelector('.initials').classList.remove('blur');
        document.querySelector('.user-avatar>img').classList.remove('blur');
        document.querySelector('.content-box').classList.remove('text-blur');
        document.querySelector('.blur-button').disabled = false;
        document.querySelector('.button-form').classList.remove('blur-button');
    }
})

